#pragma once

#include <cmath>
#include <limits>

#define PEAK_DISTRIBUTION_WIDTH 64
#define HIGH_FREQUENCY_BOUND_HZ 4000

class ClapDetector
{
private:
    const size_t sampleRate_;
    const float timeConstantSeconds_;
    const float thresholdDB_;
    const float clapLengthSeconds_;
    const float highLowRelationThreshold_;
    const float majorityThreshold_;

    const float clapLengthSamples_;
    
    float environmentBiasDB_;

    size_t heatUpSamplesCount_;
    float averagingFilterValue_;

    size_t samplesProcessed_;

    bool isAboveZeroDetector_;
    float peakAtDetectorValue_;
    size_t crossZeroAtDetectorPosition_;
    size_t firstOverThresholdPeakPosition_, lastOverThresholdPeakPosition_;

    bool isInClapEvent_;
    size_t peakDistribution_[PEAK_DISTRIBUTION_WIDTH];

    bool IsHeatedUp() const
    {
        return samplesProcessed_ > heatUpSamplesCount_;
    }

public:
    ClapDetector(const size_t sampleRate, const float timeConstantSeconds, const float thresholdDB,
        const float clapLengthSeconds, const float highLowRelationThreshold,
        const float majorityThreshold)
        : sampleRate_(sampleRate)
        , timeConstantSeconds_(timeConstantSeconds)
        , thresholdDB_(thresholdDB)
        , clapLengthSeconds_(clapLengthSeconds)
        , highLowRelationThreshold_(highLowRelationThreshold)
        , majorityThreshold_(majorityThreshold)
        , clapLengthSamples_(sampleRate * clapLengthSeconds)
        , heatUpSamplesCount_(timeConstantSeconds * sampleRate)
        , averagingFilterValue_(1.0f / heatUpSamplesCount_)
        , samplesProcessed_(0)
        , peakAtDetectorValue_(0)
        , crossZeroAtDetectorPosition_(0)
        , firstOverThresholdPeakPosition_(0)
        , lastOverThresholdPeakPosition_(0)
    {
        ResetEvent();
    }

    void ProcessBlock(const std::vector<float>& inputSamples)
    {
        if (IsHeatedUp())
        {
            RunClapDetection(inputSamples);
        }

        AdjustEnvironmentBias(inputSamples);

        samplesProcessed_ += inputSamples.size();
    }

private:
    float SampleToDecibels(const float sample) const
    {
        return 20 * std::log10(sample + std::numeric_limits<float>::epsilon());
    }

    void ResetEvent()
    {
        isInClapEvent_ = false;
        memset(&peakDistribution_, 0, sizeof(peakDistribution_));
    }

    void AdjustEnvironmentBias(const std::vector<float>& inputSamples)
    {
        for (const float sample : inputSamples)
        {
            environmentBiasDB_ = environmentBiasDB_ * (1 - averagingFilterValue_) +
                SampleToDecibels(std::abs(sample)) * averagingFilterValue_;
        }
    }

    void RunClapDetection(const std::vector<float>& inputSamples)
    {
        size_t samplePosition = samplesProcessed_;

        for (const float sample : inputSamples)
        {
            samplePosition++;

            const bool isAboveZero = sample >= 0;
            if (isAboveZero != isAboveZeroDetector_)
            {
                isAboveZeroDetector_ = isAboveZero;

                size_t peakWidthSamples = samplePosition - crossZeroAtDetectorPosition_;
                crossZeroAtDetectorPosition_ = samplePosition;

                float peakAtDetectorValueDB = SampleToDecibels(peakAtDetectorValue_);
                if (peakAtDetectorValueDB - environmentBiasDB_ >= thresholdDB_)
                {
                    lastOverThresholdPeakPosition_ = samplePosition;

                    if (isInClapEvent_ == false)
                    {
                        isInClapEvent_ = true;
                        firstOverThresholdPeakPosition_ = samplePosition;
                    }

                    if (peakWidthSamples < PEAK_DISTRIBUTION_WIDTH)
                    {
                        peakDistribution_[peakWidthSamples]++;
                    }
                }

                peakAtDetectorValue_ = 0;
            }

            if (isInClapEvent_ &&
                samplePosition - firstOverThresholdPeakPosition_ > clapLengthSamples_ * 2)
            {
                isInClapEvent_ = false;
                DetectEvent();
                //RecordEvent();
                ResetEvent();
            }

            float absSample = std::abs(sample);
            if (absSample > peakAtDetectorValue_)
            {
                peakAtDetectorValue_ = absSample;
            }
        }
    }

    void DetectEvent()
    {
        float highFrequencyPeakCount = 0;
        float lowFrequencyPeakCount = 1;

        size_t highFrequencyBound = sampleRate_ / HIGH_FREQUENCY_BOUND_HZ;

        for (size_t n = 0; n < PEAK_DISTRIBUTION_WIDTH; n++)
        {
            if (n < highFrequencyBound)
            {
                highFrequencyPeakCount += peakDistribution_[n];
            } else
            {
                lowFrequencyPeakCount += peakDistribution_[n];
            }
        }

        float highLowRelation = highFrequencyPeakCount / lowFrequencyPeakCount;
        float majorityRelation = highFrequencyPeakCount / clapLengthSamples_;
        float clapLengthSeconds = static_cast<float>(lastOverThresholdPeakPosition_ - firstOverThresholdPeakPosition_) / sampleRate_;

        if (highLowRelation > highLowRelationThreshold_ &&
            majorityRelation > majorityThreshold_ &&
            clapLengthSeconds < clapLengthSeconds_)
        {
            std::cout << "Peak detected at: " << firstOverThresholdPeakPosition_ << std::endl;
        }

        //std::cout << firstOverThresholdPeakPosition_ << ". H/L: " << highLowRelation << " MAJORITY: " << majorityRelation << " LENGTH: " << clapLengthSeconds << " s" << std::endl;
    }

    void RecordEvent()
    {
        std::cout << "Peak distribution for clap at sample position: " << firstOverThresholdPeakPosition_ 
            << " width environment BIAS: " << environmentBiasDB_ << " DB" << std::endl;

        for (size_t n = 0; n < PEAK_DISTRIBUTION_WIDTH; n++)
        {
            if (n < 2)
            {
                std::cout << n << ": " << peakDistribution_[n] << std::endl;
            } else
            {
                float relation = std::numeric_limits<float>::infinity();

                if (peakDistribution_[n - 1] != 0)
                {
                    relation = (static_cast<float>(peakDistribution_[n - 1]) - 
                        static_cast<float>(peakDistribution_[n])) / static_cast<float>(peakDistribution_[n - 1]);
                }

                std::cout << n << ": " << peakDistribution_[n] << " relation: " << relation << std::endl;
            }
        }
    }
};
