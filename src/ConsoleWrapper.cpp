#include <string.h>

#include <iostream>
#include <memory>
#include <vector>

#include "ClapDetector.h"

const size_t SampleRate = 16000;
const float TimeConstantSeconds = 3.0f;
const float ClapPeakThresholdDB = 25.0f;
const float ClapLengthSeconds = 0.25f;
const float HighLowRelationThreshold = 0.5;
const float MajorityThreshold = 0.0;

std::vector<float> ConvertInt16ToFloat(const std::vector<uint8_t>& sourceBytes)
{
    std::vector<float> result;

    const auto* sourcePointer = sourceBytes.data();

    for (size_t n = 0; n < sourceBytes.size() / sizeof(int16_t); n++)
    {
        uint16_t ushortSample = *sourcePointer;
        sourcePointer++;
        ushortSample += (*sourcePointer) * 0x100;
        sourcePointer++;

        float floatSample = static_cast<int16_t>(ushortSample) / 32768.0;

        result.push_back(floatSample);
    }

    return result;
}

void StartStream(size_t processingBlockSizeSamples)
{
    ClapDetector clapDetector(SampleRate, TimeConstantSeconds, ClapPeakThresholdDB, 
        ClapLengthSeconds, HighLowRelationThreshold, MajorityThreshold);

    std::vector<uint8_t> inputByteBuffer;

    while (true)
    {
        int stdinChar = getchar();

        if (stdinChar == EOF)
        {
            break;
        }

        inputByteBuffer.push_back(static_cast<uint8_t>(stdinChar));

        if (inputByteBuffer.size() >= processingBlockSizeSamples)
        {
            auto inputSamples = ConvertInt16ToFloat(inputByteBuffer);
            clapDetector.ProcessBlock(inputSamples);

            inputByteBuffer.clear();
        }
    }
}

int main()
{
    StartStream(64);

    return 0;
}
